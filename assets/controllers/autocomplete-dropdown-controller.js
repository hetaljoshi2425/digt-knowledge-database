// assets/controllers/autocomplete-dropdown-controller.js
import { Controller } from '@hotwired/stimulus';
import { useDispatch } from 'stimulus-use';

/* stimulusFetch: 'lazy' */
export default class extends Controller {
    static values = {
        search: String
    }

    connect()
    {
        useDispatch(this);
    }

    onSearchChanged(event)
    {
        let element = $(this.element).find("input[type=hidden]");
        this.searchValue = element.value;

        if (this.searchValue === '') {
            element.trigger('change');
        }

        /* Use this event, to submit forms dynamically on search change */
        this.dispatch('async:submitted')
    }

    submitOnRemove(event)
    {
        // Workaround, so turbo-permanent doesn't have to be disabled
        const node = this;
        setTimeout(function () {
            node.dispatch('async:submitted')
        }, 300);
    }
}