// assets/controllers/list-view-controller.js
import { Controller } from '@hotwired/stimulus';

/* stimulusFetch: 'lazy' */
export default class extends Controller {
    static targets = [
        "turboFrame",
    ];

    searchDelayTimer;

    connect()
    {
    }

    prev()
    {
        const currentPage = $("#" + this.turboFrameTarget.id + " #list_view_current_page");
        currentPage[0].value = parseInt(currentPage[0].value) - 1;
    }

    next()
    {
        const currentPage = $("#" + this.turboFrameTarget.id + " #list_view_current_page");
        currentPage[0].value = parseInt(currentPage[0].value) + 1;
    }

    async submit()
    {
        const $form = $("#" + this.turboFrameTarget.id + " form");
        $form[0].requestSubmit();
    }

    autoSubmit(event)
    {
        this.submit();
    }

    searchTrigger(event)
    {
        const node = this;
        clearTimeout(this.searchDelayTimer);
        this.searchDelayTimer = setTimeout(function () {
            node.autoSubmit(event);
        }, 500)
    }
}