// assets/controllers/Work/D01_Digt_AG/invinit/price_table_controller.js
import { Controller } from '@hotwired/stimulus';

/* stimulusFetch: 'lazy' */
export default class extends Controller {

    connect()
    {
        const addPriceTableFormDeleteLink = (priceTableDiv) => {
            // create new elements
            const removeFormButtonDiv = document.createElement('td');
            const removeFormButton = document.createElement('button');

            // set button text
            removeFormButton.innerText = 'Remove';

            // add formatting css classes
            removeFormButtonDiv.classList.add('col-md-2', 'col-4');
            removeFormButton.classList.add('p-btn');

            // add button element to formatting div
            removeFormButtonDiv.appendChild(removeFormButton);

            // add div to price list item
            priceTableDiv.append(removeFormButtonDiv);

            // remove item if 'remove' is clicked
            removeFormButton.addEventListener('click', (e) => {
                e.preventDefault();

                $(priceTableDiv).next('.error-row').remove();
                priceTableDiv.remove();
            })
        };

        $('#add-price-table-btn').click(function (e) {
            // get item list
            var list = $(jQuery(this).attr('data-list-selector'));

            // Try to find the counter of the list or use the length of the list
            var counter = list.data('widget-counter') || list.children().length;

            // grab the prototype template
            var newWidget = list.attr('data-prototype');

            // replace the "__name__" used in the id and name of the prototype
            newWidget = newWidget.replace(/__name__/g, counter);
            // Increase the counter
            counter++;
            // And store it, the length cannot be used if deleting widgets is allowed
            list.data('widget-counter', counter);

            // create a new list element
            var item = $.parseHTML(newWidget)[1];

            // add 'remove' button to item
            addPriceTableFormDeleteLink(item);

            // add element to the list
            list.append(item);
        });

        const items = document.querySelectorAll('.price-table-list-item');

        items.forEach((item) => {
            addPriceTableFormDeleteLink(item);
        });
    }

    calcInclVat(event)
    {
        // get vatRate
        var vatRate = $('#product_form_vatRate')[0].value;

        // loop through prices excl. vat
        $('.price-excl-vat').each(function (index, element) {
            // calc price incl. vat
            var priceInclVat = element.value * (vatRate / 100 + 1);
            var priceInclVatRounded = Math.round(priceInclVat * 100) / 100;

            // set price incl. vat at current index
            $('.price-incl-vat').get(index).value = priceInclVatRounded;
        })
    }
}