
# Installation

### Clone git repository
```
git@gitlab.com:jonathan.caplunik/digt-x-2-0-templates.git
```

### Install PHP dependencies
```
composer install
```

### Setup local MySQL Database
After you've set up your database and configured it in your `.env.local` run:
```
php bin/console make:migration
```
and
```
php bin/console doctrine:migrations:migrate
```
to generate the DB tables.
### Install JavaScript dependencies
```
yarn install
```

### Build JavaScript
```
yarn encore dev
```

### Generate test user
```
php bin/console doctrine:fixtures:load
```
This will generate a test user for you to be able to log into the app.

email: `test.user`

password: `test`

# Implementation
after you've logged in, you will see the templates we created for you to use.

### `List View`
... is a fully functional `FormType` that allows rendering data tables 
and can be configured as you'll see in the backend.

### Templates 
The rest are templates which act as
guideline on how to implement view components what HTML structures and CSS classes to use.

### Modals
For a modal view reference click on "Search" in the left-hand menu.


