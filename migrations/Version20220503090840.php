<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220503090840 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE app_user (id INT AUTO_INCREMENT NOT NULL, UserEmail VARCHAR(400) NOT NULL, UserName VARCHAR(400) NOT NULL, UserLastName VARCHAR(400) DEFAULT NULL, UserUsername VARCHAR(400) NOT NULL, UserDigtUId INT DEFAULT NULL, UserCreated DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_user_auth (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, googleAuthenticatorSecret VARCHAR(255) DEFAULT NULL, is_verified TINYINT(1) NOT NULL, fkUser INT NOT NULL, UNIQUE INDEX UNIQ_2A30A3A5E7927C74 (email), UNIQUE INDEX UNIQ_2A30A3A58D68ADD3 (fkUser), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE document (id INT AUTO_INCREMENT NOT NULL, knowledge_id INT DEFAULT NULL, path VARCHAR(255) DEFAULT NULL, original_name VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT NULL, INDEX IDX_D8698A76E7DC6902 (knowledge_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ext_log_entries (id INT AUTO_INCREMENT NOT NULL, action VARCHAR(8) NOT NULL, logged_at DATETIME NOT NULL, object_id VARCHAR(64) DEFAULT NULL, object_class VARCHAR(191) NOT NULL, version INT NOT NULL, data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', username VARCHAR(191) DEFAULT NULL, INDEX log_class_lookup_idx (object_class), INDEX log_date_lookup_idx (logged_at), INDEX log_user_lookup_idx (username), INDEX log_version_lookup_idx (object_id, object_class, version), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB ROW_FORMAT = DYNAMIC');
        $this->addSql('CREATE TABLE knowledge_category (id INT AUTO_INCREMENT NOT NULL, knowledge_category_status TINYINT(1) NOT NULL, knowledge_category_name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE knowledge_db (id INT AUTO_INCREMENT NOT NULL, entry_status TINYINT(1) DEFAULT 1 NOT NULL, fk_user INT DEFAULT NULL, entity_created DATETIME NOT NULL, entity_title VARCHAR(255) NOT NULL, entry_tags VARCHAR(255) DEFAULT NULL, entry_abstract VARCHAR(255) DEFAULT NULL, entry_details LONGTEXT DEFAULT NULL, writer VARCHAR(255) DEFAULT NULL, entity_updated DATETIME NOT NULL, fkCompany INT NOT NULL, fkDepartment INT NOT NULL, fkCategory INT NOT NULL, UNIQUE INDEX UNIQ_BB11B27A599FBFC0 (fkCompany), UNIQUE INDEX UNIQ_BB11B27AAE8594D7 (fkDepartment), UNIQUE INDEX UNIQ_BB11B27A7B5DA7C6 (fkCategory), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_company (id INT AUTO_INCREMENT NOT NULL, user_company_status TINYINT(1) NOT NULL, user_company_name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_department (id INT AUTO_INCREMENT NOT NULL, user_department_status TINYINT(1) NOT NULL, user_department_name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE app_user_auth ADD CONSTRAINT FK_2A30A3A58D68ADD3 FOREIGN KEY (fkUser) REFERENCES app_user (id)');
        $this->addSql('ALTER TABLE document ADD CONSTRAINT FK_D8698A76E7DC6902 FOREIGN KEY (knowledge_id) REFERENCES knowledge_db (id)');
        $this->addSql('ALTER TABLE knowledge_db ADD CONSTRAINT FK_BB11B27A599FBFC0 FOREIGN KEY (fkCompany) REFERENCES user_company (id)');
        $this->addSql('ALTER TABLE knowledge_db ADD CONSTRAINT FK_BB11B27AAE8594D7 FOREIGN KEY (fkDepartment) REFERENCES user_department (id)');
        $this->addSql('ALTER TABLE knowledge_db ADD CONSTRAINT FK_BB11B27A7B5DA7C6 FOREIGN KEY (fkCategory) REFERENCES knowledge_category (id) ON DELETE RESTRICT');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE app_user_auth DROP FOREIGN KEY FK_2A30A3A58D68ADD3');
        $this->addSql('ALTER TABLE knowledge_db DROP FOREIGN KEY FK_BB11B27A7B5DA7C6');
        $this->addSql('ALTER TABLE document DROP FOREIGN KEY FK_D8698A76E7DC6902');
        $this->addSql('ALTER TABLE knowledge_db DROP FOREIGN KEY FK_BB11B27A599FBFC0');
        $this->addSql('ALTER TABLE knowledge_db DROP FOREIGN KEY FK_BB11B27AAE8594D7');
        $this->addSql('DROP TABLE app_user');
        $this->addSql('DROP TABLE app_user_auth');
        $this->addSql('DROP TABLE document');
        $this->addSql('DROP TABLE ext_log_entries');
        $this->addSql('DROP TABLE knowledge_category');
        $this->addSql('DROP TABLE knowledge_db');
        $this->addSql('DROP TABLE user_company');
        $this->addSql('DROP TABLE user_department');
    }
}
