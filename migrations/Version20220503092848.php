<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220503092848 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE knowledge_db DROP INDEX UNIQ_BB11B27A7B5DA7C6, ADD INDEX IDX_BB11B27A7B5DA7C6 (fkCategory)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE knowledge_db DROP INDEX IDX_BB11B27A7B5DA7C6, ADD UNIQUE INDEX UNIQ_BB11B27A7B5DA7C6 (fkCategory)');
    }
}
