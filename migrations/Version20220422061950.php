<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220422061950 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE knowledge_category (id INT AUTO_INCREMENT NOT NULL, knowledge_category_status TINYINT(1) NOT NULL, knowledge_category_name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE knowledge_db (id INT AUTO_INCREMENT NOT NULL, entry_status TINYINT(1) NOT NULL, fk_company INT NOT NULL, fk_department INT NOT NULL, fk_category INT NOT NULL, fk_user INT NOT NULL, entity_created DATETIME NOT NULL, entity_title VARCHAR(255) NOT NULL, entry_tags VARCHAR(255) DEFAULT NULL, entry_abstract VARCHAR(255) DEFAULT NULL, entry_details LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_company (id INT AUTO_INCREMENT NOT NULL, user_company_status TINYINT(1) NOT NULL, user_company_name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_department (id INT AUTO_INCREMENT NOT NULL, user_department_status TINYINT(1) NOT NULL, user_department_name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE knowledge_category');
        $this->addSql('DROP TABLE knowledge_db');
        $this->addSql('DROP TABLE user_company');
        $this->addSql('DROP TABLE user_department');
    }
}
