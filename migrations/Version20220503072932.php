<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220503072932 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE knowledge_db ADD UNIQUE INDEX UNIQ_BB11B27A599FBFC0 (fkCompany)');
        $this->addSql('ALTER TABLE knowledge_db  ADD UNIQUE INDEX UNIQ_BB11B27AAE8594D7 (fkDepartment)');
        $this->addSql('ALTER TABLE knowledge_db  ADD UNIQUE INDEX UNIQ_BB11B27A7B5DA7C6 (fkCategory)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
       
    }
}
