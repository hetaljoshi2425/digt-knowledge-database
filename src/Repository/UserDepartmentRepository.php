<?php

namespace App\Repository;

use App\Entity\UserDepartment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method UserDepartment|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserDepartment|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserDepartment[]    findAll()
 * @method UserDepartment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserDepartmentRepository extends ServiceEntityRepository
{
    private $em;
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $em)
    {
        parent::__construct($registry, UserDepartment::class);
        $this->em = $em;
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(UserDepartment $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(UserDepartment $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function getDepartmentList($search): array
    {
        $queryBuilder = $this->em->createQueryBuilder()
            ->select('d')
            ->from('App\Entity\UserDepartment', 'd')
            ->andWhere("d.user_department_name LIKE :search")
            ->andWhere("d.user_department_status = 1")
            ->setParameter('search', "%$search%")
            ->getQuery();

        return $queryBuilder->getArrayResult();
    }

    // /**
    //  * @return UserDepartment[] Returns an array of UserDepartment objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserDepartment
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
