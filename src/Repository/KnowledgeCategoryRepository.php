<?php

namespace App\Repository;

use App\Entity\KnowledgeCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method KnowledgeCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method KnowledgeCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method KnowledgeCategory[]    findAll()
 * @method KnowledgeCategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class KnowledgeCategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, KnowledgeCategory::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(KnowledgeCategory $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(KnowledgeCategory $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function getCategoryList($search): array
    {
        $queryBuilder = $this->createQueryBuilder('kc')
            ->select('kc.id', 'kc.knowledge_category_status', 'kc.knowledge_category_name')
            ->andWhere("kc.knowledge_category_name LIKE :search")
            ->andWhere("kc.knowledge_category_status = 1")
            ->setParameter('search', "%$search%")
            ->getQuery();

        return $queryBuilder->getArrayResult();
    }

    // /**
    //  * @return KnowledgeCategory[] Returns an array of KnowledgeCategory objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('k.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?KnowledgeCategory
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
