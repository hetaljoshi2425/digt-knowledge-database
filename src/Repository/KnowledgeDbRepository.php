<?php

namespace App\Repository;

use App\Entity\KnowledgeDb;
use App\Entity\UserCompany;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method KnowledgeDb|null find($id, $lockMode = null, $lockVersion = null)
 * @method KnowledgeDb|null findOneBy(array $criteria, array $orderBy = null)
 * @method KnowledgeDb[]    findAll()
 * @method KnowledgeDb[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class KnowledgeDbRepository extends ServiceEntityRepository
{
    private $manager;
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, KnowledgeDb::class);
        $this->manager = $manager;
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(KnowledgeDb $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(KnowledgeDb $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
    * @return KnowledgeDb[] Returns an array of KnowledgeDb objects
    */
    public function findById($value)
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.id = :val')
            ->setParameter('val', $value)
            ->orderBy('k.id', 'DESC')
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    
    public function findOneById($value): ?KnowledgeDb
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.id = :val')
            ->setParameter('val', $value)
            ->orderBy('k.id', 'DESC')
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    
    public function getKnowledgeDb($search): array
    {
        $queryBuilder = $this->createQueryBuilder('u')
            ->select('u.id', 'u.username')
            ->andWhere("u.username LIKE :search OR u.name LIKE :search OR u.lastName LIKE :search")
            ->setParameter('search', "%$search%")
            ->getQuery();

        return $queryBuilder->getArrayResult();
    }
 
    public function getKnowledgeDbList(): QueryBuilder
    {
        // return $this->createQueryBuilder('k')
        //     ->select('k');

        $queryBuilder = $this->createQueryBuilder('k');
            return $queryBuilder
                    ->addSelect('k.entityTitle','k.entryAbstract','k.writer','k.entryTags','k.id', 'c.knowledge_category_name as CategoryName','d.user_department_name as DeptName','cm.user_company_name as CompanyName')
                    ->join('\App\Entity\KnowledgeCategory', 'c')
                    ->andWhere('k.fkCategory = c')
                    ->join('\App\Entity\UserDepartment', 'd')
                    ->andWhere('k.fkDepartment = d')
                    ->join('\App\Entity\UserCompany', 'cm')
                    ->andWhere('k.fkCompany = cm');
    }
}
