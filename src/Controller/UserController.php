<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\Type\UserDetailForm;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private ObjectRepository $userRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
    ) {
        $this->entityManager = $entityManager;

        $this->userRepository = $this->entityManager->getRepository(User::class);
    }

    public function editUserDetails(Request $request): Response
    {
        $form = $this->createForm(UserDetailFormType::class);
    }

    public function getDropdownUserList(Request $request): Response
    {
        $search = $request->query->get('q');
        $users = $this->userRepository->getUsers($search);

        $result = "";
        foreach ($users as $wa) {
            $result .= '<li class="list-group-item" role="option" data-autocomplete-value="'
                . $wa['id']
                . '">' . $wa['username'] . '</li>';
        }

        return new Response($result);
    }
}