<?php

namespace App\Controller;

use App\Form\Dashboard\exampleProject\Type\UserListType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends AbstractController
{
    public function showDashboard(): Response
    {
        $list = $this->createForm(UserListType::class);

        return $this->renderForm('_home.html.twig', [
            'list' => $list,
        ]);
    }
}
