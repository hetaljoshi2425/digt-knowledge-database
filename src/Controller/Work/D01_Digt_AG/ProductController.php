<?php

namespace App\Controller\Work\D01_Digt_AG;

use App\Form\Work\D01_Digt_AG\Type\ProductFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends AbstractController
{
    public function editProduct(Request $request): Response
    {
        $data = [];
        $form = $this->createForm(ProductFormType::class, $data);
        return $this->renderForm('Work/D01_Digt_AG/product/product_edit.html.twig',[
            'form' => $form,
        ]);
    }
}