<?php

namespace App\Controller;

use App\Entity\KnowledgeCategory;
use App\Entity\KnowledgeDb;
use App\Entity\UserCompany;
use App\Entity\Document;
use App\Entity\UserDepartment;
use Doctrine\ORM\EntityManagerInterface;
use App\Form\Type\KnowledgeFormType;
use Doctrine\Persistence\ObjectRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\Dashboard\exampleProject\Type\KnowledgeListType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class KnowledgeController extends AbstractController
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager) {
        $this->entityManager = $entityManager;

        $this->knowledgeDbRepository = $this->entityManager->getRepository(KnowledgeDb::class);

        $this->userCompanyRepository = $this->entityManager->getRepository(UserCompany::class);

        $this->userDepartmentRepository = $this->entityManager->getRepository(UserDepartment::class);

        $this->knowledgeCategoryRepository = $this->entityManager->getRepository(KnowledgeCategory::class);
    }

    #[Route('/knowledge', name: 'app_knowledge')]
    public function index(): Response
    {
        $list = $this->createForm(KnowledgeListType::class);

        return $this->renderForm('knowledge/index.html.twig', [
            'knowlegde_list' => $list,
        ]);
    }

    public function getDropdownKnowledgeCompanyList(Request $request): Response
    {
        $search = $request->query->get('q');
        $companies = $this->userCompanyRepository->getCompanyList($search);

        $result = "";
        foreach ($companies as $wa) {
            $result .= '<li class="list-group-item" role="option" data-autocomplete-value="'
                . $wa['id']
                . '">' . $wa['user_company_name'] . '</li>';
        }

        return new Response($result);
    }

    public function getDropdownKnowledgeDepartmentList(Request $request): Response
    {
        $search = $request->query->get('q');
        $departments = $this->userDepartmentRepository->getDepartmentList($search);

        $result = "";
        foreach ($departments as $wa) {
            $result .= '<li class="list-group-item" role="option" data-autocomplete-value="'
                . $wa['id']
                . '">' . $wa['user_department_name'] . '</li>';
        }

        return new Response($result);
    }

    public function getDropdownKnowledgeCategoryList(Request $request): Response
    {
        $search = $request->query->get('q');
        $categories = $this->knowledgeCategoryRepository->getCategoryList($search);

        $result = "";
        foreach ($categories as $wa) {
            $result .= '<li class="list-group-item" role="option" data-autocomplete-value="'
                . $wa['id']
                . '">' . $wa['knowledge_category_name'] . '</li>';
        }

        return new Response($result);
    }

    public function openKnowledge(Request $request): Response
    {
        $id = $request->query->get('knowledgeId');
        $knowledge_details = $this->knowledgeDbRepository->findOneById($id);
        return $this->renderForm('knowledge/knowledge_open.html.twig',[
            'knowledge_details' => $knowledge_details,
        ]);
    }

    /**
     * @Route("/knowledge/add/", name="add_knowledge")
     */
    public function addKnowledge(Request $request): Response
    {
        $knowledgeData = new KnowledgeDb();
        $form = $this->createForm(KnowledgeFormType::class, $knowledgeData);
        $form->handleRequest($request);
        $data = $form->getData();

        if($form->isSubmitted() && $form->isValid()){
            try {
                $knowledgeData->setEntityTitle($data->getEntityTitle());
                $knowledgeData->setEntryTags($data->getEntryTags());
                $knowledgeData->setFkCompany($data->getFkCompany());
                $knowledgeData->setFkDepartment($data->getFkDepartment());
                $knowledgeData->setFkCategory($data->getFkCategory());
                $knowledgeData->setWriter($data->getWriter());
                $knowledgeData->setEntryAbstract($data->getEntryAbstract());
                $knowledgeData->setEntryDetails($data->getEntryDetails());

                $this->entityManager->persist($knowledgeData);
                $this->entityManager->flush();
                
                /** @var UploadedFile $pic */
                $pics = $form->get('document')->getData();
                foreach ($pics as $pic) {
                    $documentData = new Document();
                    $documentData->setFile($pic);
                    $documentData->setKnowledge($knowledgeData);
                    $extensions = 'doc,docx,odf,pdf,rtf,xls,xlsx,csv,zip,jpg,jpeg,png,pdf,doc,docx,tiff,tif,mp3,mp4,mpeg,mov,html,css,psd,psp,ppt,pptx,tar,txt,wav,heic';
                    $documentData->upload($extensions);
                    $this->entityManager->persist($documentData);
                    $this->entityManager->flush();
                }
                return $this->redirectToRoute('app_knowledge');
            } catch (\Throwable $th) {
            }
        }else{
            $errors = $form->getErrors();
        }
        return $this->renderForm('knowledge/knowledge_add.html.twig',[
            'form' => $form,
            'errors' => $errors,
        ]);
    }

    /**
     * @Route("/knowledge/edit/{id}", name="edit_knowledge")
    */
    public function editKnowledge($id, Request $request): Response
    {
        $knowledgeDetails = $this->knowledgeDbRepository->findById(['id' => $id]);

        if (!$knowledgeDetails) {
            throw $this->createNotFoundException(
            'There are no knowledge data with the following id: ' . $id
            );
        }

        $form = $this->createForm(KnowledgeFormType::class, $knowledgeDetails);
        $form->handleRequest($request);
        $knowledgedata = $form->getData();

        if($form->isSubmitted() && $form->isValid()){
            if ($request->isMethod('post')) {
                try{
                    $knowledgeDetails->setEntityTitle($knowledgedata->getEntityTitle());
                    $knowledgeDetails->setEntryTags($knowledgedata->getEntryTags());
                    $knowledgeDetails->setFkCompany($knowledgedata->getFkCompany());
                    $knowledgeDetails->setFkDepartment($knowledgedata->getFkDepartment());
                    $knowledgeDetails->setFkCategory($knowledgedata->getFkCategory());
                    $knowledgeDetails->setWriter($knowledgedata->getWriter());
                    $knowledgeDetails->setEntryAbstract($knowledgedata->getEntryAbstract());
                    $knowledgeDetails->setEntryDetails($knowledgedata->getEntryDetails());
        
                    $this->entityManager->persist($knowledgeDetails);
                    $this->entityManager->flush();

                    /** @var UploadedFile $files */
                    $files = $form->get('document')->getData();
                    foreach ($files as $file) {
                        $documentData = new Document();
                        $documentData->setFile($file);
                        $documentData->setKnowledge($knowledgeDetails);
                        $extensions = 'doc,docx,odf,pdf,rtf,xls,xlsx,csv,zip,jpg,jpeg,png,pdf,doc,docx,tiff,tif,mp3,mp4,mpeg,mov,html,css,psd,psp,ppt,pptx,tar,txt,wav,heic';
                        $documentData->upload($extensions);
                        $this->entityManager->persist($documentData);
                        $this->entityManager->flush();
                    }
                    return $this->redirectToRoute('app_knowledge');
                }catch (\Throwable $th) {
                    echo "<pre>";dump($th);die;
                }
            }
        }
        else{
            $errors = $form->getErrors();
        }
        return $this->renderForm('knowledge/knowledge_edit.html.twig',[
            'form' => $form,
            'knowledgeDetails' => $knowledgeDetails,
            'errors' => $errors,
        ]);
    }
}
