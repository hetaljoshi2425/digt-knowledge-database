<?php

namespace App\Entity;

use App\Repository\DocumentRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity(repositoryClass=DocumentRepository::class)
 */
class Document
{
    public const ALLOWED_MIME = 'application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.oasis.opendocument.text,application/pdf,application/xls';
    public const ALLOWED_EXTENSION = 'doc,docx,odf,pdf,rtf,xls,xlsx,csv,zip,jpg,jpeg,png,pdf,doc,docx,tiff,tif,mp3,mp4,mpeg,mov,html,css,psd,psp,ppt,pptx,tar,txt,wav,heic';

    protected $protected = false;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $path;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $originalName;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\KnowledgeDb", inversedBy="documents", fetch="EXTRA_LAZY", cascade={"persist"})
     */
    public $knowledge;

    /**
     * @var UploadedFile
     * @Assert\File(maxSize="40M")
     */
    public $file;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $createdAt;

    public function __construct($protected = false)
    {
        $this->path = '';
        $this->protected = $protected;
        $this->createdAt = new \DateTime();
    }

    public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir() . '/' . $this->path;
    }

    public function getWebPath()
    {
        return null === $this->path
            ? null
            : $this->path;
    }

    protected function getUploadRootDir()
    {
        return __DIR__ . '/../../../' . ($this->protected ? 'protected' : 'web') . '/' . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'uploads/documents';
    }

    public function __toString()
    {
        return is_string($this->path) ? $this->path : '';
    }

    public function setProtected($protected)
    {
        $this->protected = $protected;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setPath(?string $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    /**
     * Get file
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Sets file
     * @param UploadedFile $file
     * @return Document
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Set knowledge
     *
     * @param \App\Entity\KnowledgeDb $knowledge
     *
     * @return Document
     */
    public function setKnowledge(\App\Entity\KnowledgeDb $knowledge = null)
    {
        $this->knowledge = $knowledge;

        return $this;
    }

    /**
     * Get knowledge
     *
     * @return \App\Entity\KnowledgeDb
     */
    public function getKnowledge()
    {
        return $this->knowledge;
    }

    public function upload($customAllowedExtensions = null)
    {
        if (null === $this->file) {
            return;
        }

        $extension = $this->getExtensionFromUploadedFile();
        if (!$this->isAllowedExtension($extension, $customAllowedExtensions)) {
            return;
        }

        $childDir = strtolower(chr(rand(65, 90)) . chr(rand(65, 90)));
        $rootRir = $this->getUploadRootDir() . '/' . $childDir;

        $filename = uniqid() . '.' . $extension;

        $this->file->move('uploads/', $filename);

        $relativePath = 'uploads/' . $filename;
        
        $this->setPath($relativePath);

        if (method_exists($this->file, 'getClientOriginalName')) {
            $this->setOriginalName($this->file->getClientOriginalName());
        }

        $this->file = null;
    }

    public function unlink()
    {
        if ($this->path && is_file($this->path)) {
            @unlink($this->path);
        }
    }

    protected function isAllowedExtension($extension, $customAllowedExtensions = null)
    {
        $allowedExtensions = explode(",", $customAllowedExtensions ?? self::ALLOWED_EXTENSION);
        return in_array(strtolower($extension), $allowedExtensions, true);
    }

    protected function getExtensionFromUploadedFile()
    {
        if (!$this->file) return null;
        if (method_exists($this->file, 'getClientOriginalName')) {
            $originalClientFilename = explode('.', $this->file->getClientOriginalName());
            $ext = end($originalClientFilename);
        } else {
            $parts = explode('.', $this->file->getFilename());
            $ext = end($parts);
        }
        return strtolower($ext);
    }


    public function setOriginalName(?string $originalName): self
    {
        $this->originalName = $originalName;

        return $this;
    }

    public function getOriginalName() : ?string
    {
        return $this->originalName;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }

}
