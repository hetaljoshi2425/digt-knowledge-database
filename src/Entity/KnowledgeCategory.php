<?php

namespace App\Entity;

use App\Repository\KnowledgeCategoryRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=KnowledgeCategoryRepository::class)
 */
class KnowledgeCategory
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $knowledge_category_status;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $knowledge_category_name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\KnowledgeDb", mappedBy="fkCategory", cascade={"persist"})
     */
    protected $knowledge;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getKnowledgeCategoryStatus(): ?bool
    {
        return $this->knowledge_category_status;
    }

    public function setKnowledgeCategoryStatus(bool $knowledge_category_status): self
    {
        $this->knowledge_category_status = $knowledge_category_status;

        return $this;
    }

    public function getKnowledgeCategoryName(): ?string
    {
        return $this->knowledge_category_name;
    }

    public function setKnowledgeCategoryName(string $knowledge_category_name): self
    {
        $this->knowledge_category_name = $knowledge_category_name;

        return $this;
    }
}
