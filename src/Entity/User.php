<?php

namespace App\Entity;

use App\Entity\Work\D04_Logistik\Invinit2\CompanyEmployee;
use App\Entity\Work\D04_Logistik\Invinit2\Log;
use App\Entity\Work\D04_Logistik\Invinit2\Reservation;
use App\Entity\Work\D04_Logistik\Invinit2\UniqueProduct;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name = "app_user")
 * @method string getUserIdentifier()
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=400, name="UserEmail")
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=400, name="UserName")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=400, nullable=true, name="UserLastName")
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=400, name="UserUsername")
     */
    private $username;

    /**
     * @ORM\Column(type="integer", length=255, name="UserDigtUId", nullable=true)
     */
    private $digtUId;

    /**
     * @ORM\Column(type="datetime", name="UserCreated", nullable=true)
     */
    private $created;

    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getDigtUId()
    {
        return $this->digtUId;
    }

    public function setDigtUId($digtUId): void
    {
        $this->digtUId = $digtUId;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function setCreated($created): void
    {
        $this->created = $created;
    }
}
