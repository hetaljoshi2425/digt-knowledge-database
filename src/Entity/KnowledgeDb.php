<?php

namespace App\Entity;

use App\Repository\KnowledgeDbRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=KnowledgeDbRepository::class)
 */
class KnowledgeDb
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean", options={"default" : 1}, nullable=false)
     */
    private $entry_status;

    /**
     * @ORM\ManyToOne(targetEntity=UserCompany::class, inversedBy="knowledge", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false, name="fkCompany")
     */
    private $fkCompany;

    /**
     * @ORM\ManyToOne(targetEntity=UserDepartment::class, inversedBy="knowledge", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false, name="fkDepartment")
     */
    private $fkDepartment;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\KnowledgeCategory", inversedBy="knowledge", cascade={"persist"})
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false, onDelete="RESTRICT", name="fkCategory")
     */
    private $fkCategory;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="id", fetch="EXTRA_LAZY")
     */
    private $fkUser;

    /**
     * @ORM\Column(type="datetime")
     */
    private $entityCreated;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $entityTitle;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $entryTags;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $entryAbstract;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $entryDetails;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $writer;

    /**
     * @ORM\Column(type="datetime")
     */
    private $entityUpdated;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Document", mappedBy="knowledge", cascade={"persist"})
     */
    protected $documents;

    public function __construct()
    {
        $this->entityCreated = new \DateTime();
        $this->entityUpdated = new \DateTime();
        $this->documents= new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEntryStatus(): ?bool
    {
        return $this->entry_status;
    }

    public function setEntryStatus(bool $entry_status): self
    {
        $this->entry_status = $entry_status;

        return $this;
    }
   
    public function getFkCompany(): ?\App\Entity\UserCompany
    {
        return $this->fkCompany;
    }

    public function setFkCompany(\App\Entity\UserCompany $fkCompany): self
    {
        $this->fkCompany = $fkCompany;

        return $this;
    }

    public function getFkDepartment(): ?\App\Entity\UserDepartment
    {
        return $this->fkDepartment;
    }

    public function setFkDepartment(\App\Entity\UserDepartment $fkDepartment): self
    {
        $this->fkDepartment = $fkDepartment;

        return $this;
    }

    public function getFkCategory(): ?\App\Entity\KnowledgeCategory
    {
        return $this->fkCategory;
    }

    public function setFkCategory(\App\Entity\KnowledgeCategory $fkCategory): self
    {
        $this->fkCategory = $fkCategory;

        return $this;
    }

    public function getFkUser(): ?int
    {
        return $this->fkUser;
    }

    public function setFkUser(int $fkUser): self
    {
        $this->fkUser = $fkUser;

        return $this;
    }

    public function getEntityCreated(): ?\DateTimeInterface
    {
        return $this->entityCreated;
    }

    public function setEntityCreated(\DateTimeInterface $entityCreated): self
    {
        $this->entityCreated = $entityCreated;

        return $this;
    }

    public function getEntityTitle(): ?string
    {
        return $this->entityTitle;
    }

    public function setEntityTitle(string $entityTitle): self
    {
        $this->entityTitle = $entityTitle;

        return $this;
    }

    public function getEntryTags(): ?string
    {
        return $this->entryTags;
    }

    public function setEntryTags(?string $entryTags): self
    {
        $this->entryTags = $entryTags;

        return $this;
    }

    public function getEntryAbstract(): ?string
    {
        return $this->entryAbstract;
    }

    public function setEntryAbstract(?string $entryAbstract): self
    {
        $this->entryAbstract = $entryAbstract;

        return $this;
    }

    public function getEntryDetails(): ?string
    {
        return $this->entryDetails;
    }

    public function setEntryDetails(?string $entryDetails): self
    {
        $this->entryDetails = $entryDetails;

        return $this;
    }

    public function getWriter(): ?string
    {
        return $this->writer;
    }

    public function setWriter(?string $writer): self
    {
        $this->writer = $writer;

        return $this;
    }

    public function getEntityUpdated(): ?\DateTime
    {
        return $this->entityUpdated;
    }

    public function setEntityUpdated(\DateTime $entityUpdated): self
    {
        $this->entityUpdated = $entityUpdated;

        return $this;
    }

    /**
     * Set documents
     *
     * @param \App\Entity\Document $documents
     *
     * @return KnowledgeDb
     */
    public function setDocuments(\App\Entity\Document $documents = null)
    {
        $this->documents = $documents;

        return $this;
    }

    /**
     * Get documents
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * Add documents
     *
     * @param \App\Entity\Document $documents
     *
     * @return KnowledgeDb
     */
    public function addDocuments(\App\Entity\Document $documents)
    {
        $this->documents[] = $documents;

        return $this;
    }

    /**
     * Remove documents
     *
     * @param \App\Entity\Document $documents
     */
    public function removeDocument(\App\Entity\Document $documents)
    {
        $this->document->removeElement($documents);
    }

    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'entryStatus' => $this->getEntryStatus(),
            'fkCompany' => $this->getFkCompany(),
            'fkDepartment' => $this->getFkDepartment(),
            'fkCategory' => $this->getFkCategory(),
            'entityTitle' => $this->getEntityTitle(),
            'entryTags' => $this->getEntryTags(),
            'entryAbstract' => $this->getEntryAbstract(),
            'entryDetails' => $this->getEntryDetails(),
            'writer' => $this->getWriter(),
            'documents' => $this->getDocuments(),
        ];
    }
}
