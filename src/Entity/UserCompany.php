<?php

namespace App\Entity;

use App\Repository\UserCompanyRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserCompanyRepository::class)
 */
class UserCompany
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $user_company_status;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $user_company_name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\KnowledgeDb", mappedBy="fkCategory", cascade={"persist"})
     */
    protected $knowledge;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserCompanyStatus(): ?bool
    {
        return $this->user_company_status;
    }

    public function setUserCompanyStatus(bool $user_company_status): self
    {
        $this->user_company_status = $user_company_status;

        return $this;
    }

    public function getUserCompanyName(): ?string
    {
        return $this->user_company_name;
    }

    public function setUserCompanyName(string $user_company_name): self
    {
        $this->user_company_name = $user_company_name;

        return $this;
    }

    public function __toString()
    {
        return $this->getUserCompanyName();
    }

}
