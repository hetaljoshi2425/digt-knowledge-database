<?php

namespace App\Entity;

use App\Repository\UserDepartmentRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserDepartmentRepository::class)
 */
class UserDepartment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $user_department_status;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $user_department_name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\KnowledgeDb", mappedBy="fkCategory", cascade={"persist"})
     */
    protected $knowledge;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserDepartmentStatus(): ?bool
    {
        return $this->user_department_status;
    }

    public function setUserDepartmentStatus(bool $user_department_status): self
    {
        $this->user_department_status = $user_department_status;

        return $this;
    }

    public function getUserDepartmentName(): ?string
    {
        return $this->user_department_name;
    }

    public function setUserDepartmentName(string $user_department_name): self
    {
        $this->user_department_name = $user_department_name;

        return $this;
    }
}
