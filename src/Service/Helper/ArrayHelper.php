<?php

namespace App\Service\Helper;

class ArrayHelper
{
    private static array $values;
    private static ?string $key;

    /**
     * Flattens multidimensional array into one-dimensional
     * if $key provided values will be saved with prefix $key
     *
     * @param array $values
     * @param $key
     * @return array
     */
    public static function flattenArray(array $values, $key = null): array
    {
        self::$values = [];
        self::$key = $key;

        self::doFlattenArray($values, '');

        return self::$values;
    }

    private static function doFlattenArray($array, $key)
    {
        foreach ($array as $k => $value) {
            if (is_array($value)) {
                self::doFlattenArray($value, $key . $k . "_");
            } else {
                if (self::$key !== null) {
                    self::$values[self::$key][$key . "$k"] = $value;
                } else {
                    self::$values[$key . "$k"] = $value;
                }
            }
        }
    }
}
