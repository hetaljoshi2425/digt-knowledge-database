<?php

namespace App\Service\Translator;

use Doctrine\ORM\EntityManagerInterface;
use Scn\DeeplApiConnector\DeeplClientFactory;
use Scn\DeeplApiConnector\Enum\LanguageEnum;

class DeeplTranslatorService
{
    public array $locales = [
        'de_CH' => LanguageEnum::LANGUAGE_DE,
        'en' => LanguageEnum::LANGUAGE_EN,
        'it_CH' => LanguageEnum::LANGUAGE_IT,
        'fr_CH' => LanguageEnum::LANGUAGE_FR,
    ];

    private EntityManagerInterface $entityManager;

    private $deepl;

    public function __construct(
        EntityManagerInterface $entityManager,
    ) {
        $this->entityManager = $entityManager;

        $this->deepl = DeeplClientFactory::create('7b43e408-5a22-4c5c-4474-63d6fb96e0d0');
    }

    public function translate(string $value, $locale): string
    {
        $language = $this->locales[$locale];
        $translatedObject = $this->deepl->translate($value, $language);
        return $translatedObject->getText();
    }
}