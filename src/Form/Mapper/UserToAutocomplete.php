<?php

namespace App\Form\Mapper;

use App\Entity\User;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\FormInterface;

class UserToAutocomplete implements DataMapperInterface
{

    public function mapDataToForms($viewData, \Traversable $forms)
    {
    }

    public function mapFormsToData(\Traversable $forms, &$viewData)
    {
        /** @var FormInterface[] $forms */
        $forms = iterator_to_array($forms);

        if ($viewData instanceof User) {
            return;
        }
        $product = $forms['autocompleteDropdown']->getParent()->getParent()->getData();

        $viewData = $product->getOwner();
    }
}