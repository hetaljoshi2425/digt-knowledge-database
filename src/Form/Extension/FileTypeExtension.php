<?php

namespace App\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FileTypeExtension extends AbstractTypeExtension
{
    public static function getExtendedTypes(): iterable
    {
        return [FileType::class];
    }

    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $fileUrl = null;
        $originalFileName = null;
        $parentData = $form->getParent()->getData();

        if ($parentData !== null) {
            $propertyName = $form->getName();
            $getProperty = 'get' . ucfirst($propertyName);

            $filePath = null;
            if (is_array($parentData)) {
                $filePath = $parentData[$propertyName] ?? null;
            } elseif (method_exists($parentData, $getProperty)) {
                $filePath = $parentData->$getProperty();
            }

            if ($filePath !== null) {
                $fileName = preg_replace('/(.*\/)+/', '', $filePath);
                $originalFileName = preg_replace('/-invinit_(\w|\d){13}\./', '.', $fileName);

                $fileUrl = $fileName;
            }
        }

        $view->vars["file_url"] = $fileUrl;
        $view->vars["file_name"] = $originalFileName;
        $view->vars["file_container_css"] = $options['file_container_css'];
        $view->vars["preview_container_css"] = $options['preview_container_css'];
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'file_container_css' => '',
            'preview_container_css' => '',
        ]);
    }
}