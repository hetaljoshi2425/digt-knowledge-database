<?php

namespace App\Form\Type;

use App\Form\DataTransformer\StatusToStringTransformer;
use App\Form\Mapper\IntegerToCheckboxToggle;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StatusToggleType extends AbstractType
{
    private StatusToStringTransformer $transformer;

    public function __construct(
        StatusToStringTransformer $transformer,
    ) {
        $this->transformer = $transformer;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
//        $builder
//            ->add('status', CheckboxType::class, [
//                'mapped' => false,
//            ]);
//
        $builder->addViewTransformer($this->transformer);
//        $builder->setDataMapper(new IntegerToCheckboxToggle());
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
            'compound' => false,
        ]);
    }
}
