<?php

namespace App\Form\Type;

use App\Service\ListViewPaginator;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Exception\MissingOptionsException;

class ListViewType extends AbstractType
{
    private Request $request;
    private ListViewPaginator $paginator;

    public function __construct(
        RequestStack $request,
        ListViewPaginator $paginator,
    ) {
        $this->request = $request->getCurrentRequest();
        $this->paginator = $paginator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->assertOptions($options);

        $params = $this->request->request->all();

        // add configured filters to form
        $filters = $options['filters'];
        foreach ($filters as $filter) {
            $class = new $filter['type']();
            if (! $class instanceof AbstractFilterType) {
                throw new UnexpectedTypeException($class, AbstractFilterType::class);
            }
            $filter['options']['required'] = false;
            $builder->add($filter['name'], $filter['type'], $filter['options']);
        }

        $pageLimiter = $params['list_view_page_limiter'] ?? $options['pagination']['limits']['default'];

        $params['list_view_page_limiter'] = $pageLimiter;

        // build paginator service with provided queryBuilder
        $this->paginator->build($options['queryBuilder'], $params, $options);

        // override form data with dynamic data set
        $builder->setData($this->paginator->getData());
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $params = $this->request->request->all();

        foreach ($options as $key => $option) {
            $view->vars[$key] = $option;
        }

        $pageLimiter = $params['list_view_page_limiter'] ?? $options['pagination']['limits']['default'];
        $view->vars['list_view_page_limiter'] = $pageLimiter;
        $view->vars['list_view_search'] = $params['list_view_search'] ?? '';
        $view->vars['list_view_page_count'] = $this->paginator->getPageCount();
        $view->vars['list_view_current_page'] = $this->paginator->getCurrentPage();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'filters' => [],
            'search_fields' => [],
            'queryBuilder' => null,
            'columns' => [
                'id',
                'name',
                'date'
            ],
            'allow_add' => true,
            'allow_edit' => true,
            'allow_remove' => false,
            'allow_search' => true,
            'allow_limit' => true,
            'allow_open' => true,
            'label' => false,
            'title' => 'List View',
            'add_btn_label' => 'Add +',
            'add_btn_path' => [
                'name' => null,
                'properties' => [
                    'entityId' => 'id',
                ]
            ],
            'edit_btn_path' => [
                'name' => null,
                'properties' => [
                    'entityId' => 'id',
                ]
            ],
            'remove_btn_path' => [
                'name' => null,
                'properties' => [
                    'entityId' => 'id',
                ]
            ],
            'open_btn_path' => [
                'name' => null,
                'properties' => [
                    'entityId' => 'id',
                ]
            ],
            'pagination' => [
                'limits' => [
                    'list' => [
                        '5',
                        '10',
                        '25',
                        '50',
                        '100',
                    ],
                    'default' => '50',
                ],
            ],
        ]);
    }

    private function assertOptions(array $options): void
    {
        if ($options['allow_add'] && $options['add_btn_path']['name'] === null) {
            throw new MissingOptionsException(
                "Missing Argument 'add_btn_path': Provide 'add_btn_path' or set 'allow_add' to false",
                $options
            );
        }

        if ($options['allow_edit'] && $options['edit_btn_path']['name'] === null) {
            throw new MissingOptionsException(
                "Missing Argument 'edit_btn_path': Provide 'edit_btn_path' or set 'allow_edit' to false",
                $options
            );
        }

        if ($options['allow_remove'] && $options['remove_btn_path']['name'] === null) {
            throw new MissingOptionsException(
                "Missing Argument 'remove_btn_path': Provide 'remove_btn_path' or set 'allow_remove' to false",
                $options
            );
        }

        if ($options['allow_open'] && $options['open_btn_path']['name'] === null) {
            throw new MissingOptionsException(
                "Missing Argument 'open_btn_path': Provide 'open_btn_path' or set 'allow_open' to false",
                $options
            );
        }

        if ($options['queryBuilder'] === null) {
            throw new MissingOptionsException(
                "Missing Argument 'queryBuilder': Unable to show data",
                $options
            );
        }
    }
}
