<?php

namespace App\Form\Type;

use App\Form\DataTransformer\DateToStringTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DatePickerType extends AbstractType
{
    private DateToStringTransformer $transformer;

    public function __construct(
        DateToStringTransformer $transformer,
    ) {
        $this->transformer = $transformer;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('date', TextType::class, [
            'label' => false,
            'attr' => [
                'class' => 'datepicker',
                'autocomplete' => 'off',
            ],
            'getter' => function ($date, FormInterface $form) {
                return $date[0];
            }
        ]);

        $builder->addViewTransformer($this->transformer);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
        ]);
    }
}
