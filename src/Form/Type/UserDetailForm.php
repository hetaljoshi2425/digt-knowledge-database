<?php

namespace App\Form\Type;

use App\Entity\Company;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class UserDetailForm extends AbstractType
{
    private UrlGeneratorInterface $router;

    public function __construct(
        UrlGeneratorInterface $router,
    ) {
        $this->router = $router;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class)
            ->add('password', TextType::class, [
                'required' => false,
                'empty_data' => '',
            ])
            ->add('digtUId', TextType::class, [
                'label' => 'DigtUID',
                'required' => false,
                'empty_data' => 0,
            ])
            ->add('company', AutocompleteDropdownType::class, [
                'entity' => Company::class,
                'action' => $this->router->generate('ajax_get_companies'),
                'label' => false,
            ]);
    }
}