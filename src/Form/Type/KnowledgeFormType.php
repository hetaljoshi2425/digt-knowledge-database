<?php

namespace App\Form\Type;

use App\Entity\KnowledgeCategory;
use App\Entity\KnowledgeDb;
use App\Entity\UserCompany;
use App\Entity\UserDepartment;
use App\Repository\UserCompanyRepository;
use App\Repository\UserDepartmentRepository;
use App\Repository\KnowledgeCategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Image;

class KnowledgeFormType extends AbstractType
{
    private UrlGeneratorInterface $router;
    private Security $security;
    private $em;

    public function __construct(UrlGeneratorInterface $router, EntityManagerInterface $em, Security $security,
    ) {
        $this->router = $router;
        $this->security = $security;
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('entityTitle', TextType::class, [
                'label' => 'Title',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Entity Title'
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Entity Title is required',
                    ]),
                ],
                'error_bubbling' => true,
            ])
            ->add('entryTags', TextType::class, [
                'label' => 'Tags',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Entry Tags'
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Entry Tags is required',
                    ]),
                ],
                'error_bubbling' => true,
            ])
            ->add('writer', TextType::class, [
                'label' => 'Writer',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Writer'
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Writer is required',
                    ]),
                ],
                'error_bubbling' => true,
            ])
            // ->add('fkCompany', ChoiceType::class, [
            //     'label' => 'Company',
            //     'required' => true,
            //     'placeholder' => 'Select Company',
            //     'choices' => $this->getCompanyData(),
            //     'constraints' => [
            //         new NotBlank([
            //             'message' => 'Company is required',
            //         ]),
            //     ],
            //     'error_bubbling' => true,
            // ])
            ->add('fkCompany', EntityType::class, [
                'class' => UserCompany::class,
                'choice_label' => 'user_company_name',
            ])
            // ->add('fkDepartment', ChoiceType::class, [
            //     'label' => 'Department',
            //     'required' => true,
            //     'placeholder' => 'Select Department',
            //     'choices' => $this->getDepartmentData(),
            //     'constraints' => [
            //         new NotBlank([
            //             'message' => 'Department is required',
            //         ]),
            //     ],
            //     'error_bubbling' => true,
            // ])
            ->add('fkDepartment', EntityType::class, [
                'class' => UserDepartment::class,
                'choice_label' => 'user_department_name',
            ])
            ->add('entryStatus', ChoiceType::class, [
                'label' => 'Entry Status',
                'required' => true,
                'placeholder' => 'Select entry Status',
                'choices' => [
                    'Yes' => 1,
                    'No' => 0
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Entry Status is required',
                    ]),
                ],
                'error_bubbling' => true,
            ])
            // ->add('fkCategory', ChoiceType::class, [
            //     'label' => 'Category',
            //     'required' => true,
            //     'placeholder' => 'Select Category',
            //     'choices' => $this->getKnowledgeCategoryData(),
            //     'constraints' => [
            //         new NotBlank([
            //             'message' => 'Category is required',
            //         ]),
            //     ],
            //     'error_bubbling' => true,
            // ])
            ->add('fkCategory', EntityType::class, [
                'class' => KnowledgeCategory::class,
                'choice_label' => 'knowledge_category_name',
            ])
            ->add('document', FileType::class,[
                'label' => 'Document',
                'required' => false,
                'mapped' => false,
                'multiple' => true,
                'attr' => [
                  'multiple' => 'multiple',
                ],
                
                'file_container_css' => 'col-md-4 col-sm-6 col-5',
                'preview_container_css' => 'col-md-2 col-sm-6 col-1',
                'error_bubbling' => true
            ])
            ->add('entryAbstract', TextareaType::class, [
                'label' => 'Abstract',
                'required' => true,
                'invalid_message' => '"{{ value }}" is not valid.',
                'attr' => array('cols' => '10', 'rows' => '10'),
                'constraints' => [
                    new NotBlank([
                        'message' => 'Abstract is required',
                    ]),
                    new Length([
                        'max' => 250,
                        'maxMessage' => 'Abstract is too long. Please use max. 250 characters',
                    ]),
                ],
                'error_bubbling' => true,
            ])
            ->add('entryDetails', TextareaType::class, [
                'label' => 'Details',
                'required' => true,
                'invalid_message' => '"{{ value }}" is not valid.',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Details is required',
                    ]),
                ],
                'error_bubbling' => true,
            ]);
    }

    public function getCompanyData()
    {
        $results = $this->em->createQueryBuilder()
            ->select('c')
            ->from('App\Entity\UserCompany', 'c')
            ->andWhere('c.user_company_status = 1')
            ->getQuery()
            ->getResult();

        $data = [];
        foreach ($results as $result){
            $data[$result->getUserCompanyName()] = $result->getId();
        }
        return $data;
    }

    public function getDepartmentData()
    {
        $results = $this->em->createQueryBuilder()
            ->select('d')
            ->from('App\Entity\UserDepartment', 'd')
            ->andWhere('d.user_department_status = 1')
            ->getQuery()
            ->getResult();

        $data = [];
        foreach ($results as $result){
            $data[$result->getUserDepartmentName()] = $result->getId();
        }
        return $data;
    }

    public function getKnowledgeCategoryData()
    {
        $results = $this->em->createQueryBuilder()
            ->select('kc')
            ->from('App\Entity\KnowledgeCategory', 'kc')
            ->andWhere('kc.knowledge_category_status = 1')
            ->getQuery()
            ->getResult();

        $data = [];
        foreach ($results as $result){
            $data[$result->getKnowledgeCategoryName()] = $result->getId();
        }
        return $data;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'error_bubbling' => true
        ]);
    }
}
