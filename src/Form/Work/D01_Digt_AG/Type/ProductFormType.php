<?php

namespace App\Form\Work\D01_Digt_AG\Type;

use App\Entity\User;
use App\Form\Mapper\UserToAutocomplete;
use App\Form\Type\AutocompleteDropdownType;
use App\Form\Type\DatePickerType;
use App\Form\Type\StatusToggleType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;

class ProductFormType extends AbstractType
{
    private UrlGeneratorInterface $router;
    private Security $security;

    public function __construct(
        UrlGeneratorInterface $router,
        EntityManagerInterface $entityManager,
        Security $security,
    ) {
        $this->router = $router;
        $this->security = $security;
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('ProductTitle', TextType::class, [
                'label' => 'Name',
                'attr' => [
                    'placeholder' => 'Name'
                ],
            ])
            ->add('status', StatusToggleType::class, [
            ])
            ->add('company', ChoiceType::class, [
                'choice_label' => 'name',
                'invalid_message' => 'That is not a valid company',
            ])
            ->add('brand', ChoiceType::class, [
                'group_by' => 'company.name',
                'choice_label' => 'name',
            ])
            ->add('owner', AutocompleteDropdownType::class, [
                'action' => $this->router->generate('dropdown_get_users'),
                'entity' => User::class,
                'label' => false,
            ])
            ->add('stockAlarmLevel', IntegerType::class, [
                'label' => 'Alarming quantity',
            ])
            ->add('category', TextType::class, [
            ])
            ->add('subCategory', TextType::class, [
                'label' => 'Subcategory',
            ])
            ->add('ean', IntegerType::class, [
                'label' => 'Primary EAN',
                'attr' => [
                    'placeholder' => 'Primary EAN'
                ],
            ])
            ->add('ean2', IntegerType::class, [
                'label' => 'Secondary EAN',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Secondary EAN'
                ],
            ])
            ->add('articleNr', IntegerType::class, [
                'label' => 'Article Nr.',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Article Nr.'
                ],
            ])
            ->add('supplierId', TextType::class, [
                'label' => 'Supplier',
                'required' => false,
                'attr' => [
                    'placeholder' => 'ID'
                ],
            ])
            ->add('supplierName', TextType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Name'
                ],
            ])
            ->add('length', NumberType::class, [
                'label' => 'Product dimensions',
                'attr' => [
                    'placeholder' => 'length'
                ],
                'scale' => 2,
            ])
            ->add('width', NumberType::class, [
                'attr' => [
                    'placeholder' => 'width'
                ],
                'scale' => 2,
            ])
            ->add('height', NumberType::class, [
                'attr' => [
                    'placeholder' => 'height'
                ],
                'scale' => 2,
            ])
            ->add('weight', NumberType::class, [
                'label' => 'Product weight',
                'attr' => [
                    'placeholder' => 'weight'
                ],
                'scale' => 2,
            ])
            ->add('variants', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'EAN,EAN,EAN'
                ],
            ])
            ->add('accessories', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'EAN,EAN,EAN'
                ],
            ])
            ->add('warranty', IntegerType::class, [
                'attr' => [
                    'placeholder' => 'XX'
                ],
            ])
            ->add('deadOnArrivalPeriod', IntegerType::class, [
                'required' => false,
                'label' => 'Dead on arrival (DoA)',
                'attr' => [
                    'placeholder' => 'XX'
                ],
            ])
            ->add('vatRate', ChoiceType::class, [
                'choices' => [
                    '7.7%' => '7.7',
                    '3.7%' => '3.7',
                    '2.5%' => '2.5',
                ],
            ])
            ->add('productPriceTables', CollectionType::class, [
                'entry_type' => ProductPriceTableType::class,
                'prototype' => true,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label' => false,
            ])
            ->add('packingLength', NumberType::class, [
                'attr' => [
                    'placeholder' => 'length'
                ],
                'label' => 'Packing dimensions',
                'scale' => 2,
            ])
            ->add('packingWidth', NumberType::class, [
                'attr' => [
                    'placeholder' => 'width'
                ],
                'scale' => 2,
            ])
            ->add('packingHeight', NumberType::class, [
                'attr' => [
                    'placeholder' => 'height'
                ],
                'scale' => 2,
            ])
            ->add('packingWeight', NumberType::class, [
                'attr' => [
                    'placeholder' => 'Packing weights'
                ],
                'scale' => 2,
            ])
            ->add('incomingShippingDuration', IntegerType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'XX'
                ],
            ])
            ->add('outgoingShippingDuration', IntegerType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'XX'
                ],
            ])
            ->add('outgoingShippingCategory', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Outgoing shipping category'
                ],
            ])
            ->add('warehouseCountry', ChoiceType::class, [
                'required' => false,
                'choices' => [
                    'Schweiz' => 'CH',
                    'Deutschland' => 'DE',
                    'Österreich' => 'AT',
                    'Frankreich' => 'FR',
                    'Italien' => 'IT',
                    'China' => 'CN',
                ]
            ])
            ->add('mainImage', FileType::class, [
                'mapped' => false,
                'required' => false,
                'row_attr' => [
                    'hidden'
                ],
                'file_container_css' => 'col-md-4 col-sm-6 col-5',
                'preview_container_css' => 'col-md-2 col-sm-6 col-1',
            ])
            ->add('image2', FileType::class, [
                'mapped' => false,
                'required' => false,
                'row_attr' => [
                    'hidden'
                ],
                'label' => 'Image 2',
                'file_container_css' => 'col-md-4 col-sm-6 col-5',
                'preview_container_css' => 'col-md-2 col-sm-6 col-1',
            ])
            ->add('image3', FileType::class, [
                'mapped' => false,
                'required' => false,
                'row_attr' => [
                    'hidden'
                ],
                'label' => 'Image 3',
                'file_container_css' => 'col-md-4 col-sm-6 col-5',
                'preview_container_css' => 'col-md-2 col-sm-6 col-1',
            ])
            ->add('image4', FileType::class, [
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '5196k',
                        'mimeTypes' => [
                            'image/*',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid image type',
                    ]),
                ],
                'row_attr' => [
                    'hidden'
                ],
                'label' => 'Image 4',
                'file_container_css' => 'col-md-4 col-sm-6 col-5',
                'preview_container_css' => 'col-md-2 col-sm-6 col-1',
            ])
            ->add('image5', FileType::class, [
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '5196k',
                        'mimeTypes' => [
                            'image/*',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid image type',
                    ])
                ],
                'row_attr' => [
                    'hidden'
                ],
                'label' => 'Image 5',
                'file_container_css' => 'col-md-4 col-sm-6 col-5',
                'preview_container_css' => 'col-md-2 col-sm-6 col-1',
            ])
            ->add('image6', FileType::class, [
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '5196k',
                        'mimeTypes' => [
                            'image/*',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid image type',
                    ])
                ],
                'row_attr' => [
                    'hidden'
                ],
                'label' => 'Image 6',
                'file_container_css' => 'col-md-4 col-sm-6 col-5',
                'preview_container_css' => 'col-md-2 col-sm-6 col-1',
            ])
            ->add('image7', FileType::class, [
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '5196k',
                        'mimeTypes' => [
                            'image/*',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid image type',
                    ])
                ],
                'row_attr' => [
                    'hidden'
                ],
                'label' => 'Image 7',
                'file_container_css' => 'col-md-4 col-sm-6 col-5',
                'preview_container_css' => 'col-md-2 col-sm-6 col-1',
            ])
            ->add('image8', FileType::class, [
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '5196k',
                        'mimeTypes' => [
                            'image/*',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid image type',
                    ])
                ],
                'row_attr' => [
                    'hidden'
                ],
                'label' => 'Image 8',
                'file_container_css' => 'col-md-4 col-sm-6 col-5',
                'preview_container_css' => 'col-md-2 col-sm-6 col-1',
            ])
            ->add('mainDataSheet', FileType::class, [
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '5196k',
                        'mimeTypes' => [
                            'application/pdf',
                            'application/x-pdf',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid PDF file',
                    ])
                ],
                'row_attr' => [
                    'hidden'
                ],
                'file_container_css' => 'col-md-4 col-sm-6 col-5',
                'preview_container_css' => 'col-md-2 col-sm-6 col-1',
            ])
            ->add('dataSheet2', FileType::class, [
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '5196k',
                        'mimeTypes' => [
                            'application/pdf',
                            'application/x-pdf',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid PDF file',
                    ])
                ],
                'row_attr' => [
                    'hidden'
                ],
                'label' => 'Additional data sheet',
                'file_container_css' => 'col-md-4 col-sm-6 col-5',
                'preview_container_css' => 'col-md-2 col-sm-6 col-1',
            ])
            ->add('safetyDataSheet', FileType::class, [
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '5196k',
                        'mimeTypes' => [
                            'application/pdf',
                            'application/x-pdf',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid PDF file',
                    ])
                ],
                'row_attr' => [
                    'hidden'
                ],
                'file_container_css' => 'col-md-4 col-sm-6 col-5',
                'preview_container_css' => 'col-md-2 col-sm-6 col-1',
            ])
            ->add('mainVideo', FileType::class, [
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '20480k',
                        'mimeTypes' => [
                            'video/*',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid video type',
                    ])
                ],
                'row_attr' => [
                    'hidden'
                ],
                'label' => 'Main video',
                'file_container_css' => 'col-md-4 col-sm-6 col-5',
                'preview_container_css' => 'col-md-2 col-sm-6 col-1',
            ])
            ->add('video2', FileType::class, [
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '20480k',
                        'mimeTypes' => [
                            'video/*',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid video type',
                    ])
                ],
                'row_attr' => [
                    'hidden'
                ],
                'label' => 'Second video',
                'file_container_css' => 'col-md-4 col-sm-6 col-5',
                'preview_container_css' => 'col-md-2 col-sm-6 col-1',
            ])
            ->add('releaseDate', DatePickerType::class, [
                'required' => false,
            ])
            ->add('minimumAge', IntegerType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'XX'
                ],
            ])
            ->add('maxQuantityPerUser', IntegerType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'XX'
                ],
            ])
            ->add('webshopUrl', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'https://...'
                ],
                'label' => 'Origin web shop url',
            ])
            ->add('originWebshopUrl', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'https://...'
                ],
                'label' => 'Origin web product url',
            ])
            ->add('taresCode', TextType::class, [
                'required' => false,
                'label' => 'TARES Code',
                'attr' => [
                    'placeholder' => 'TARES Code'
                ],
            ])
            ->add('tariCode', TextType::class, [
                'required' => false,
                'label' => 'TARI Code',
                'attr' => [
                    'placeholder' => 'TARI Code'
                ],
            ])
            ->add('energyClass', ChoiceType::class, [
                'required' => false,
                'choices' => [
                    'A' => 'A',
                    'A+++' => 'A+++',
                    'A++' => 'A++',
                    'A+' => 'A+',
                    'B' => 'B',
                    'C' => 'C',
                    'D' => 'D',
                    'E' => 'E',
                    'F' => 'F',
                    'G' => 'G',
                ],
            ])
            ->add('energyLabelUrl', TextType::class, [
                'required' => false,
                'label' => 'Energy label',
                'attr' => [
                    'placeholder' => 'https://...'
                ],
            ])
            ->add('hazardStatements', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Hazard statements'
                ],
            ])
            ->add('precautionaryStatements', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Precautionary statements'
                ],
            ])
            ->add('productOrigin', ChoiceType::class, [
                'required' => false,
                'choices' => [
                    'Schweiz' => 'CH',
                    'Deutschland' => 'DE',
                    'Österreich' => 'AT',
                    'Frankreich' => 'FR',
                    'Taiwan' => 'TW',
                    'China' => 'CN',
                    'USA' => 'US',
                ]
            ]);

        $builder->get('owner')->setDataMapper(new UserToAutocomplete());
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'error_bubbling' => false,
        ]);
    }
}
