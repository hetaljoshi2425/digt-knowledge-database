<?php

namespace App\Form\Work\D01_Digt_AG\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductPriceTableType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('priceKey', ChoiceType::class, [
                'label' => false,
            ])
            ->add('valueExclVat', NumberType::class, [
                'scale' => 2,
                'label' => false,
                'attr' => ['placeholder' => 'Value excl. vat'],
                'required' => false,
            ])
            ->add('valueInclVat', NumberType::class, [
                'scale' => 2,
                'label' => false,
                'attr' => ['placeholder' => 'Value incl. vat'],
                'required' => false,
            ])
            ->add('currency', ChoiceType::class, [
                'label' => false,
                'choices' => [
                    'CHF' => 'CHF',
                    'EUR' => 'EUR',
                    'USD' => 'USD',
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'error_bubbling' => false,
        ]);
    }
}