<?php

namespace App\Form\Dashboard\exampleProject\Type;

use App\Entity\KnowledgeCategory;
use App\Entity\KnowledgeDb;
use App\Entity\UserCompany;
use App\Entity\UserDepartment;
use App\Form\Type\DropdownFilterType;
use App\Form\Type\ListViewType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use Faker\Provider\ar_JO\Company;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class KnowledgeListType extends AbstractType
{
    private Request $request;
    private UrlGeneratorInterface $urlGenerator;

    private ObjectRepository $KnowledgeDbRepository;

    public function __construct(
        RequestStack $request,
        EntityManagerInterface $entityManager,
        UrlGeneratorInterface $urlGenerator,
    ) {
        $this->request = $request->getCurrentRequest();
        $this->urlGenerator = $urlGenerator;

        $this->knowledgeDbRepository = $entityManager->getRepository(KnowledgeDb::class);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $qb = $this->knowledgeDbRepository->getKnowledgeDbList();
        // dd($qb->getQuery()->getResult());
        $builder
            ->add('knowledgedb', ListViewType::class, [
                // These are the necessary options for a basic list view
                'allow_add' => false,
                'allow_edit' => false,
                'allow_open' => true,
                'queryBuilder' => $qb,
                'columns' => [
                    'entityTitle' => 'Title',
                    'entryAbstract' => 'Abstact',
                    'CompanyName' => 'Company',
                    'DeptName' => 'Department',
                    'CategoryName' => 'Category',
                    'writer' => 'Writer',
                    'entryTags' => 'Tags'                   
                ],
                'search_fields' => [
                    'k.entityTitle',
                    'k.entryAbstract',
                    'k.entryTags',
                    'k.writer'
                ],
                'filters' => [
                    [
                        'type' => DropdownFilterType::class,
                        'name' => 'Company',
                        'field' => 'k',
                        'options' => [
                            'entity' => UserCompany::class,
                            'action' => $this->urlGenerator->generate('dropdown_get_knowledge_company_list'),
                            'label' => 'Company',
                            'width' => '30%',
                        ],
                    ],
                    [
                        'type' => DropdownFilterType::class,
                        'name' => 'Department',
                        'field' => 'k',
                        'options' => [
                            'entity' => UserDepartment::class,
                            'action' => $this->urlGenerator->generate('dropdown_get_knowledge_department_list'),
                            'label' => 'Department',
                            'width' => '30%',
                        ],
                    ],
                    [
                        'type' => DropdownFilterType::class,
                        'name' => 'Category',
                        'field' => 'k',
                        'options' => [
                            'entity' => KnowledgeCategory::class,
                            'action' => $this->urlGenerator->generate('dropdown_get_knowledge_category_list'),
                            'label' => 'Category',
                            'width' => '30%',
                        ],
                    ],
                ],
                'open_btn_path' => [
                    'name' => 'knowledge_open',
                    'properties' => [
                        'knowledgeId' => 'id', // id is property of the entity or a provided column from queryBuilder DQL
                    ],
                ],
            ]);
    }
}