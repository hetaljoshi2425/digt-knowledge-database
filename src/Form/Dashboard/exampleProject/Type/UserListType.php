<?php

namespace App\Form\Dashboard\exampleProject\Type;

use App\Entity\User;
use App\Form\Type\DatePickerFilterType;
use App\Form\Type\DropdownFilterType;
use App\Form\Type\ListViewType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class UserListType extends AbstractType
{
    private Request $request;
    private UrlGeneratorInterface $urlGenerator;

    private ObjectRepository $userRepository;

    public function __construct(
        RequestStack $request,
        EntityManagerInterface $entityManager,
        UrlGeneratorInterface $urlGenerator,
    ) {
        $this->request = $request->getCurrentRequest();
        $this->urlGenerator = $urlGenerator;

        $this->userRepository = $entityManager->getRepository(User::class);
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $qb = $this->userRepository->getUserList();

        $builder
            ->add('users', ListViewType::class, [
                // These are the necessary options for a basic list view
                'allow_add' => false,
                'allow_edit' => true,
                'allow_open' => false,
                'queryBuilder' => $qb,
                'columns' => [
                    'name' => 'Name',
                    'lastName' => 'Last name',
                    'email' => 'Email',
                ],
                'search_fields' => [
                    'u.name',
                    'u.lastName',
                    'u.email',
                ],
                'filters' => [
                    [
                        'type' => DropdownFilterType::class,
                        'name' => 'User',
                        'field' => 'u',
                        'options' => [
                            'entity' => User::class,
                            'action' => $this->urlGenerator->generate('dropdown_get_users'),
                            'label' => 'User',
                            'width' => '80%',
                        ],
                    ],
                    [
                        'type' => DatePickerFilterType::class,
                        'name' => 'Date',
                        'field' => 'u.created',
                        'options' => [
                            'label' => 'Date',
                            'width' => '20%',
                        ],
                    ],
                ],
                'edit_btn_path' => [
                    'name' => 'work_digt_ag_product_edit',
                    'properties' => [
                        'productId' => 'id', // id is property of the entity or a provided column from queryBuilder DQL
                    ],
                ],
            ]);
    }
}