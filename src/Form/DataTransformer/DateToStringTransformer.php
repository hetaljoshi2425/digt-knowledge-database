<?php

namespace App\Form\DataTransformer;

use Exception;
use DateTime;
use Symfony\Component\Form\DataTransformerInterface;

class DateToStringTransformer implements DataTransformerInterface
{

    public function transform($value): ?array
    {
        if ($value === null) {
            return null;
        }

        return [$value->format('d.m.Y')];
    }

    /**
     * @throws Exception
     */
    public function reverseTransform($value): DateTime
    {
        return new DateTime($value['date']);
    }
}